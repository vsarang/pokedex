import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';

import styles from './pokeball-spinner.scss';

const PokeballSpinner = (props) => {
  return (
    <div
      className={
        classNames(styles.pokeballSpinner, {
          [styles.grow]: props.grow
        })
      }
    >
      <img
        className={
          classNames(styles.pokeball, {
            [styles.hide]: props.hide
          })
        }
        src={ '/pokeball.png' }
      />
    </div>
  );
};

PokeballSpinner.propTypes = {
  hide: PropTypes.bool,
  grow: PropTypes.bool
};

export default PokeballSpinner;
